# coding: utf-8
# use python 3.6

"""
    This module uses the html files (of the gesetze) and splits them to paragraphs
    and stores them to (splittext folder as individual paragraphs textfiles) or 
    (database entries (not implemented)).
"""

import os
import codecs
import re

PATH_HTML_FILES = "../Corpus/gesetze_en/source_html/"
SOURCE_ENCODING = "iso-8859-1"
DATABASE_URL = "../Databases/corpus_en.db"
TABLE_LAW_TEXT = "law_text"
TABLE_LAW_TITLE = "law_title"
SPLIT_TEXT_PATH = "../Corpus/gesetze_en/splittext/"


def create_db():
    import sqlite3
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    c.execute('''CREATE TABLE %s (law_name text, section text, p_number integer, p_name text, law_text text)''' % TABLE_LAW_TEXT)
    conn.commit()
    c.execute('''CREATE TABLE %s (law_name text, section text, p_name text, law_title text)''' % TABLE_LAW_TITLE)
    conn.commit()
    conn.close()


def law_text_to_db(law_name, section_number, sub_section, p_name, text):
    import sqlite3
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    t = (law_name, section_number, sub_section, p_name, text)
    c.execute("INSERT INTO %s VALUES (?,?,?,?,?)" % TABLE_LAW_TEXT, t)
    conn.commit()
    conn.close()

    
def law_title_to_db(law_name, section_number, p_name, title):
    import sqlite3
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    t = (law_name, section_number, p_name, title)
    c.execute("INSERT INTO %s VALUES (?,?,?,?)" % TABLE_LAW_TITLE, t)
    conn.commit()
    conn.close()


def read_file(html_file="../Corpus/gesetze_en/source_html/freiz.html"):
    """
    Inputs:
        html_file: File path of html file
    Output:
        Normalized unicode text of the html file
    """    
    f = codecs.open(html_file, "rb", SOURCE_ENCODING)
    file_text = f.read()
    import unicodedata
    return unicodedata.normalize("NFKD", file_text)


def yield_p(html_file):
    """
    Inputs: 
        html_file: A HTML file (from the Gestze) in the form of a string
    
    Output: 
        An iterator providing every p element's text
    """
    from bs4 import BeautifulSoup
    html_file = read_file(html_file)
    soup = BeautifulSoup(html_file, 'html.parser')
    text_in_container = soup.find(id="paddingLR12")
    p_parts = text_in_container.find_all("p", recursive=False)

    for p in p_parts:
        text = p.get_text()
        if text is "":
            continue
        if text == "table of contents":
            continue
        yield p


def write_file(law_name, section_number, sub_section, p):
    if not os.path.exists(SPLIT_TEXT_PATH  + law_name):
        os.makedirs(SPLIT_TEXT_PATH  + law_name)

    f = codecs.open(SPLIT_TEXT_PATH  + law_name + "/" + \
    section_number + "." + str(sub_section), "w", "utf-8")
    f.write(p)
    f.close()


def run(file_path, file_name):
    """
    Inputs:
        file_path: The path of the input HTML file
        file_name: Is the file_name / official (as used in the URLs) acronym of 
                the law ordinance
    
    Outputs:
        None, but causes a side effect
    
    Side effect:
        Writes text files with the splittext files
    """
    sections = []
    titles = []
    current_section = ""
    redundant = True
    for p in yield_p(file_path):
        p_name = p.a['name']
        #Check if the p is a heading or is law text
        if(p.has_attr('style')):
            for br in p.find_all("br"):
                br.replace_with("\n")
            #Some DEBUG checks to print exceptions to the next if
            if("section" in p.get_text() and not "Chapter" in p.get_text() ):
                if(not re.search("\Asection[s]? ", p.get_text().lower() ) or \
                re.search("\A§ ", p.get_text())):
                    print("\nLaw: ", file_name)
                    print(p.get_text())
                    continue
            
            if(re.search("\Asection[s]? ", p.get_text().lower())\
            or re.search("\A§ ", p.get_text())):
                #Since the section has started, there is nothing to ignore
                redundant = False
                if(not "\n" in p.get_text()):
                 number = p.get_text().split(" ")[1]
                 title = ""
                else:
                 number, title = p.get_text().split("\n", 1)
                 number = number.split(" ")[-1]
                current_section = number
                law_title_to_db(file_name, current_section, \
                                p_name, title)
                sub_section = 0
            continue
        
        #Ignore the text before the start of the first section
        if(redundant):
            continue
        
        
        #What is this for?!
        #if(p.get_text()[0:5] == "Part "):
        #continue
        
        #write_file(file_name, current_section, sub_section,\
        #   p.get_text().strip())
        law_text_to_db(file_name, current_section, sub_section, \
                       p_name, p.get_text().strip())
        sub_section += 1
         
if __name__ == "__main__":
    create_db()
    from os import listdir
    files = listdir(path=PATH_HTML_FILES)
    for f in files:
        run(PATH_HTML_FILES + f, f.split(".")[-2])
        
