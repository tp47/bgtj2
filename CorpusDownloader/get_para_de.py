# coding: utf-8
# Uses Python 3!


"""
    This module is used to read the german gesetze and split the files according the newlines occurances.
    Also, this command should record the section titles and save it to tge corpus
"""

#Various configs
CORPUS_DIR = "gesetze_de/"
SOURCE_ENCODING = "utf-8"
#~ DATABASE_URL = "../corpus_de.db"
DATABASE_URL = "../RamDB/corpus_de.db"
TABLE_NAME = "law_text"
TITLES_NAME = "section_titles"
#~ SPLIT_FILE_DIR = "../RamDB/gesetze_de_splittext/"
SPLIT_FILE_DIR = "../gesetze_de_splittext/"
DEBUG = True

import re
import textwrap
import os

def create_db():
    import sqlite3
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    c.execute('''CREATE TABLE %s (law_name text, section text, p_number integer, law_text text)''' % TABLE_NAME)
    c.execute('''CREATE TABLE %s (law_name text, section text, section_title text)''' % TITLES_NAME)
    conn.commit()
    conn.close()

def write_to_db(law_name, section_number, sub_section, text):
    t = (law_name, section_number, sub_section, text)
    c.execute("INSERT INTO %s VALUES (?,?,?,?)" % TABLE_NAME, t)
    conn.commit()


def write_title_to_db(law_name, section_number, title):
    t = (law_name, section_number, title)
    c.execute("INSERT INTO %s VALUES (?,?,?)" % TITLES_NAME, t)


def write_to_file(law_name, section_number, sub_section, p):
    import codecs
    if not os.path.exists(SPLIT_FILE_DIR + law_name):
        os.makedirs(SPLIT_FILE_DIR + law_name)
    try:
        # Hitting none type over here!
        file_name = section_number+"."+str(sub_section)
        file_path =  os.path.join(SPLIT_FILE_DIR , law_name, file_name)
    except:
        path_list = [SPLIT_FILE_DIR, law_name, "/", section_number,".",str(sub_section)]
        file_path = ''.join(filter(None, path_list))
        print ("Error file path: "+ file_path)
        exit()
    f = codecs.open(file_path, "w", "utf-8")
    f.write(p)
    f.close()


def subdirs(path):
    """Yield directory names not starting with '.' under given path."""
    import os 
    for entry in os.scandir(path):
        if not entry.name.startswith('.') and entry.is_dir():
            yield entry.name


def read_file(md_file):
    """
    Inputs:
        md_file: File path of markdown file
    Output:
        Normalized unicode text of the markdown file
    """    
    import codecs
    f = codecs.open(md_file, "rb", SOURCE_ENCODING)
    file_text = f.read()
    import unicodedata
    return unicodedata.normalize("NFKD", file_text)


def yield_markdown():
    """
        Yeilds a tuple with the law's name and the law's text
    """
    #~ for law_name in ["stgb"]:
    for law_name in subdirs(CORPUS_DIR):
        md_file = CORPUS_DIR + law_name + "/index.md"
        md_text = read_file(md_file)
        yield (law_name, md_text)


def split_md(md_text):
    """
        Splits the markdown file to constituents
    """
    md_text_split = re.split("\n\n[\n]*", md_text)
    #Find all the range of the header, return without it
    start_index = 0
    for i,ele in enumerate(md_text_split):
        if re.match("\A[#]+ §", ele):
            #if § is not found in the text, program will/should crash
            start_index = i
            break
    for ele in md_text_split[i:]:
        yield ele
    
def detect_section(md_text):
    """
        Detects section numbers, additionally also kills (returns null) text which is 
        a heading, but not a section heading
    """
    # Detect if it is a markdown heading
    if not md_text[0] == "#":
        # False when it is not a heading
        return False
    md_text = re.split("[#]+ ", md_text)[1]
    # Detect if the paragraph symbol is present
    if not md_text[0] == "§":
        # This None is returned to signiffy that it is  an unimportant heading
        return None 
    # Split with spaces, second element is always section number
    md_split = md_text.split(" ", 2)
    if not len(md_split) == 3:
        section_number, title = md_split[1], ""
    else:
        section_number, title = md_split[1], md_split[2]
    return (section_number, title)
    
    
        
def run() :
    """
        Takes the markdown file and splits according to sections
    """
    law_count = 0
    error_count = 0
    sentence_count = 0
    section_count = 0
    for law_name, md_text in yield_markdown():
        print("Working on: "+law_name)
        #~ print(ignore_head(md_text)[0])
        #~ print("\n\n")
        current_section = "0"
        prev_section = "0"
        sub_section_number = 0
        current_title = ""
        for ele in split_md(md_text):
            #~ print(textwrap.dedent(ele))
            # Kill None type ele
            if not ele:
                continue
                print("We have a problem")
            try:
                section_tuple = detect_section(ele)
            except Exception as error:
                print("ERROR at " + current_section + "." + \
                    str(sub_section_number))
                print(str(error))
                error_count += 1
                continue
            # If detect_section returns Null, it is not relavent
            if section_tuple == None:
                continue
            if not section_tuple == False:
                # Not the same as previous section
                # set section number and title, then skip to the next 
                # entry
                current_section, current_title = section_tuple
                print("§ "+current_section)
                write_title_to_db(law_name, current_section,  \
                  current_title)
                section_count += 1
                sub_section_number = 0
                continue
            write_to_db(law_name, current_section, \
              sub_section_number, ele)
            if not current_section:
                print("Hell")
            #~ write_to_file(law_name, current_section, \
                    #~ sub_section_number, ele)
            sub_section_number += 1
            sentence_count += 1
        
        law_count += 1
        
        
        
    print("\n\nStats:")
    print("Types of laws:", law_count)
    print("Total sections of law:", section_count)
    print("Sucessful Paragraphs:", sentence_count)
    print("Failed Paragraphs:", error_count)


    
if __name__ == '__main__':
    # Write functions to backup the database!!!
    # Remove the database from RamDB
    # !!! TmpFS should be mounted onto RamDB
    import os
    os.system("rm ../RamDB/corpus_de.db")
    
    import sqlite3
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()

    create_db()
    run()
    conn.close()

    
