# coding: utf-8


import pickle
from gensim.models import Doc2Vec
import random

picklefile = open("law_docs_en.pickle",'rb')
documents = pickle.load(picklefile)model = Doc2Vec(documents, size=100, window=8, min_count=5, workers=4)
model = Doc2Vec(documents, size=100, window=8, min_count=5, workers=4)


model.save("law_en.model")
train_i = int(len(documents) * 0.5)

train_data_i = random.sample(range(len(documents)), train_i)
train_data = [documents[i] for i in train_data_i]
    
for epoch in range(10):
    model.train(train_data, total_examples=model.corpus_count, epochs=epoch)
    model.alpha -= 0.002
    model.min_alpha = model.alpha
    print(f"Finished epoch {epoch}")
    
    
model.docvecs.most_similar()
model.docvecs.most_similar("murder on purpose")
model.predict_output_word("murder")
model.predict_output_word("killed")
model.predict_output_word("section")
print(type(model.syn0))
model.docvecs.most_similar("0")
model.docvecs.most_similar("vvg")
model.docvecs.most_similar(positive=['murder','accidental'])
get_ipython().run_line_magic('save', '0~ doc2vec.py')
get_ipython().run_line_magic('save', 'doc2vec.py 0~')
get_ipython().run_line_magic('save', 'doc2vec.py 0')
get_ipython().run_line_magic('save', 'doc2vec.py')
get_ipython().run_line_magic('save', 'doc2vec ~0/')
