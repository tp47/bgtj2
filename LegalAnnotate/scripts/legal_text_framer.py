"""
Framing a legal sentence to it's high level constituents
"""

import legaltext
import os
from spacy_pipeline import nlp

bad_sentences = []

FRAME_SUBCHILD_LIMIT = 5

def remove_unwanted(sent):
    sent_split = sent.split(" ", 1)
    lafw = sent_split[0][-1]  # last_alpa_first_word
    if lafw.isalpha() or lafw == ":":
        return sent
    while not (lafw.isalpha() or lafw == ":"):
        if len(sent_split) == 1:
            break
        sent = sent_split[1]
        sent_split = sent.split(" ", 1)
        if not sent_split[0]:
            continue
        try:
            lafw = sent_split[0][-1]  # last_alpa_first_word
        except:
            import pdb
            pdb.set_trace()
    # while not cleaned[0].isupper():
        # else:
        #     print("Problem removing enumeration")
        #     print("Sentence type not supported?")
    print(f"Cleaned:\n{sent}", end="\n\n")
    return sent.strip()


def check_resplit(obj):
    """
    If a given frame has many children,
    split agian!
    """
    grandchildren = len(list(obj.children))
    # 3 has been arbitrarly choosen
    if grandchildren > FRAME_SUBCHILD_LIMIT:
        return True
    else:
        return False


def make_frames(children, root, resplit = True, level=1):
    """
    For children of a parse tree's root,
    create indivdual frames
    Parameters:
    children: list of spacy parse tree children
    root: root node of parse (sub)tree
    resplit: break large subtrees recurrsively
    level: number of resplits, to tag the root node
    """
    frames = []
    for c in children:
        # Check if replitting is needed
        if check_resplit(c) and resplit:
            gframes = make_frames(c.children, c,
                                  resplit = resplit,
                                  level = level+1)
            frames.extend(gframes)
            print(f"Resplit results: {gframes}")
            continue
        loc = c.i
        subtree = list(c.subtree)
        subphrase = ""
        # first is a flag to store first loc of subtree
        first = True
        # Iterate over nodes of a subtree
        for node in subtree:
            if first:
                loc = node.i
                first = False
            token = node.orth_
            subphrase += " "+token
            header = (c, c.pos_, loc)
            my_frame = (header, subphrase)
        frames.append(my_frame)

    # Now to insert the root at the right place
    list_pos = 0
    # Loop till you find the right index
    for i,f in enumerate(frames):
        h, _ = f
        _, _, pos = h
        list_pos = i
        if pos > root.i:
            break
    header = (root, f"{level*'*'}_{root.pos_}", root.i)
    # ROOT_ signifies that this is the root word
    root_frame = (header, root.text)
    # Insert root after {list_pos}
    frames.insert(list_pos, root_frame)

    return frames

def get_frames(nlp, sent):
    """
    get_frames accepts a sentence,
    splits the sentence to specific frames.
    To get the frames, the steps used are:
    1. Run the dependency parser from Spacy
    2. Identify root word of sentence; Root word is usuaully a legal act
    3. Make a list of frames (children subtrees of the root word)
    frames is a List of tokens which have been framed
    """
    # cleaned = remove_unwanted(sent)
    cleaned = sent
    doc = nlp(cleaned)
    sentences = []
    # Iterate over sentences
    for sent in doc.sents:
        # root_pos = sent.root.i
        children = sent.root.children
        # Iterate over subtrees of a sentence
        frames = make_frames(children, sent.root)
        sentences.append(frames)
    return sentences

# gchildren = list(c.children) #grandchildren
# if len(gchildren) > 3:
#     print("Promote these:")
#     for gc in c.children:
#         print(gc.text)
#     print("~")
            
def record_errors(loc, error):
    bad_sentences.append((loc, error))


def frame_replace(frames, rule_words):
    start_loc = -1
    for i, myframe in enumerate(frames):
        meta, text = myframe
        if not isinstance(text, str):
            # Somehow, some instances of text are of type Spacy.token
            text = text.text
        if text.strip() == rule_words[0] and frames[i+1] == rule_words[1]:
            start_loc = i
            break
    if start_loc == -1:
        return frames
    new_frames = []
    rule_pos = []
    for i, myframe in enumerate(frames):
        if i < start_loc or i > start_loc + len(rule_words):
            new_frames.append(myframe)
        else:
            # Something about joining frames
            # We have to find out if there is ACT_VERB present in the text
            header, text = myframe
            root_text, pos, loc = header
            rule_pos.append(pos)
            if i != start_loc:
                if pos == "ROOT_VERB":
                    print(f"{root_text} is the ROOT_VERB")
        print(f"Replace from {start_loc}")


# def frame_replace(frames, rule_words):
#     words = rule_words
#     start_loc = 0
#     for i, f in enumerate(frames):
#         meta, text = myframe
#     return new_frames

def frame_joiner(frames):
    """
    When tokens following a join rule is found,
    the corresponding following tokens
    are all combined to form one token
    """
    shall_rules = [
        "may only be",
        "shall be",
        "shall apply"
    ]

    # Get only the text from the list of tokens of the frames
    frames_text = [str(f).strip() for _, f in frames]

    # Let's make that list into a string
    frames_text = " ".join(frames_text)
    rule_matches = list(rule in frames_text for rule in shall_rules)

    # If none of the rules match, send back original copy
    if set(rule_matches) == set([False]):
        return frames

    rule_type = rule_matches.index(True)
    rule_words = shall_rules[rule_type].split(' ')
    new_frames = frame_replace(frames, rule_words)
    return new_frames


def remove_simple_numbering(text):
    """
    Remove numbered text only with
    the pattern (123), as these are not
    enumerated.
    """
    if text[0] == '(' and text[1].isdigit():
        return text.split(')', 1)[1].strip()
    else:
        return text


if __name__ == "__main__":
    # import pdb
    # pdb.set_trace()
    # en_nlp = spacy.load('en')
    stgb_paragraphs = legaltext.get_section("stgb")
    para_buffer = []
    for idx, row in enumerate(stgb_paragraphs):
        code, para, sub_para, para_id, text = row
        os.system('clear')
        print("="*50)
        print(f"#{idx} Original:\n{text}", end="\n\n")
        if text[-1] != ".":
            print("Warning! This is not a complete sentence")
            para_buffer.append(text)
            continue
        elif para_buffer != []:
            print("Text is para_buffer:")
            para_buffer.append(text)
            text = "\n".join(para_buffer)
            print('-'*5 + "Combined Paragraph" + '-'*5)
            print(text)
            print('-'*25)
            para_buffer = []
        else:
            # text = remove_simple_numbering(text)
            text = remove_unwanted(text)

        if "(repealed)" in text:
            print("Warning! Repealed.")
            continue
        sentences = get_frames(nlp, text)
        new_sentences = []
        for frames in sentences:
            new_frames = frame_joiner(frames)
            new_sentences.append(new_frames)
        print("\nFrames:")
        for s in sentences:
            print('-'*5 + 'Sentence' + '-'*5)
            for f in s:
                print(f, end="\n\n")
        print('-'*20)
        _ = input("Press enter to continue...")
