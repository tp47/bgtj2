# Roadmap
* To work with Parse tree structures, make a GUI in which Parse trees are visualized.
* Different Parse tree grammers can be created and the corresponding matched sentences should be returned.
* This is the first step of matching rules for find specific semantics in the law corpus.
