#!/usr/bin/env python
 
import sys
import xapian

def search(query_string, start = 0, end = 50):                                                                                          
    """
            Important values to be extracted:
            1. rank
            2. percent
            3. docid?
                *. FInd a method to map the docid to the <p> tag id??
            4. document.get_data()
        """
    try:
        #~ database = xapian.Database('./gesetze')
        database = xapian.Database('de_db/default')
        enquire = xapian.Enquire(database)
        qp = xapian.QueryParser()
        #~ stemmer = xapian.Stem("english")
        stemmer = xapian.Stem("german")
        qp.set_stemmer(stemmer)
        qp.set_database(database)
        qp.set_stemming_strategy(xapian.QueryParser.STEM_SOME)
        query = qp.parse_query(query_string)
     
        enquire.set_query(query)
        matches = enquire.get_mset(start, end)
        
        for m in matches:
            data = m.document.get_data()
            data = data.decode("utf-8")
            url = ""
            sample = ""
            for i, d in enumerate(data.split("\n")):
                if i == 0:
                    url = d.split("url=")[1]
                if i == 1:
                    sample = d.split("sample=")[1]
                if i > 1:
                    break
            yield(url, sample)
        #~ print("%i results found." % matches.get_matches_estimated())
        #~ print("Results 1-%i:" % matches.size())
                           
        #~ for m in matches:
            #~ print("%i: %i%% docid=%i [%s]" % (m.rank + 1, m.percent, m.docid,
         #~ m.document.get_data()))
    except Exception as error:
        print >> sys.stderr, "Exception: %s" % str(error)

if __name__ == '__main__':
    n_result= search(str.join(' ', sys.argv[1:]))
    for result in n_result:
        print(result)
    #~ print(result_tuple)
    
