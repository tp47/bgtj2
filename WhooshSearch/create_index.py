from whoosh.analysis import RegexTokenizer, StopFilter
from whoosh.index import create_in
# from whoosh.fields import *
from whoosh.analysis import StemmingAnalyzer
from whoosh.fields import SchemaClass, TEXT, KEYWORD, ID, STORED
import sqlite3
import os

INDEX_NAME = 'titlesindex'
DATABASE_URL = "../Databases/corpus_en.db"
TABLE_NAME = "law_title"

analysis = RegexTokenizer() | StopFilter()


class TextSchema(SchemaClass):
    # law_title = TEXT(analyzer=stem_ana, stored=True)
    law_name = TEXT(stored=True)  # Version without the stemming analyser
    section = TEXT(stored=True)
    p_number = ID(stored=True)
    p_name = TEXT(stored=True)
    law_text = TEXT(analyzer=analysis, stored=True)  # law_title
    # KEYWORDS


class TitleSchema(SchemaClass):
    # law_title = TEXT(analyzer=stem_ana, stored=True)
    law_title = TEXT(stored=True)  # Version without the stemming analyser
    para_n = ID(stored=True)
    p_number = TEXT(stored=True)
    content = TEXT(analyzer=analysis, stored=True)  # law_title
    # KEYWORDS

# Need to create a directory called indexdir for ix to be initialized.


if not os.path.exists(INDEX_NAME):
    os.mkdir(INDEX_NAME)
ix = create_in(INDEX_NAME, TitleSchema)# TextSchema)


conn = sqlite3.connect(DATABASE_URL)
c = conn.cursor()

# write_index = True
# if write_index:
writer = ix.writer()
LAWS_FROM_DB = c.execute("SELECT * from %s" % TABLE_NAME)
for i, law in enumerate(LAWS_FROM_DB):
    print(law)
    # writer.add_document(law_name=law[0], section=law[1],
    #                     p_number=str(i), p_name=law[3],
    #                     law_text=law[4])
    writer.add_document(law_title=law[0], para_n=law[1],
                        p_number=law[2], content=law[3])
    # Output structure:
    # law-title, para-#, sub-para#, law-text
writer.commit()
