import pickle
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
import nltk

pickle_file = open("pickles/lawsyn_sim_reverse.pickle", 'rb')
lawsyn_sim_reverse = pickle.load(pickle_file)
pickle_file = open("pickles/law_syns_reverse.pickle", 'rb')
law_syns_reverse = pickle.load(pickle_file)
pickle_file = open("pickles/law_words.pickle", 'rb')
all_law_words = pickle.load(pickle_file)

stop_words = set(stopwords.words('english'))


def get_synonyms_tokens(tokens):
    """Takes tokens and returns synoymns, hypernyms and lemmas
    Takes 2.94s for running 1000 instances"""

    def get_wordnet_pos(treebank_tag):
        """
        return WORDNET POS compliance to WORDENT lemmatization (a,n,r,v,s)
        """
        if treebank_tag.startswith('J'):
            return wn.ADJ
        elif treebank_tag.startswith('V'):
            return wn.VERB
        elif treebank_tag.startswith('N'):
            return wn.NOUN
        elif treebank_tag.startswith('R'):
            return wn.ADV
        elif treebank_tag.startswith('S'):
            return wn.ADJ_SAT
        else:
            # As default pos in lemmatization is Noun
            return wn.NOUN

    def in_law(token):
        if token in all_law_words:
            return True
        return False

    def token_expander(term, _pos, filter_syn_level=1):
        if term in stop_words:
            return None
        all_terms = []
        word_syns = wn.synsets(term, pos=_pos)
        for term in word_syns:
            term_word, pos, syn_level = term.name().split('.')
            # print(term_word)
            if int(syn_level) > filter_syn_level:
                continue
            if in_law(term_word):
                all_terms.append(term_word)
            for hyper in term.hypernyms():
                hyper_word, pos, syn_level = hyper.name().split('.')
                # print(hyper_word)
                if int(syn_level) > filter_syn_level:
                    continue
                if in_law(hyper_word):
                    all_terms.append(hyper_word)
            for lemma in term.lemmas():
                lemma_word = lemma.name()
                if in_law(lemma_word):
                    all_terms.append(lemma_word)
        return list(set(all_terms))

    pos_tokens = nltk.pos_tag(tokens)
    return [token_expander(word, get_wordnet_pos(pos)) for word, pos in pos_tokens]


def _2dlist_to_query_lang(input_tokens, generator, force_or):
    # Probably change the approach to this
    law_words_list = generator(input_tokens)
    # Above is a 3d list.
    # Level 1: tokens
    # Level 2: synonym of token
    # Level 3: similar words
    query_string = ""
    token_first = True
    concatenater = ' OR ' if force_or else ' AND '
    for syn_group in law_words_list:
        if not syn_group: continue
        syns_string = "("
        syn_first = True
        for synonym in syn_group:
            if not synonym: continue
            if syn_first:
                syns_string += synonym
                syn_first = False
            else:
                syns_string += " OR " + synonym
        syns_string += ")"
        if token_first:
            query_string += syns_string
            token_first = False
        else:
            query_string += concatenater + syns_string
    return query_string


def query2lawsyns(input_tokens, force_or=False):
    return _2dlist_to_query_lang(input_tokens, get_synonyms_tokens, force_or)


def get_lawsimslist(tokens):
    law_words_list = []
    for t in tokens:
        if t in stop_words:
            continue
        law_words_group = [[t]]
        if t in lawsyn_sim_reverse.keys():
            law_syns = lawsyn_sim_reverse[t]
            # print("Similar word to law synonymn: ", law_syns)
            for syn in law_syns:
                if syn[0] in law_syns_reverse.keys():
                    law_words = law_syns_reverse[syn[0]]
                    # print("Syn: ", syn[0], "; Law word: ", law_words)
                    # law_words for synonyms are extended to a single list
                    law_words_group.append(law_words)
        law_words_list.append(law_words_group)
    return law_words_list


def _3dlist_to_query_lang(input_tokens, generator, force_or):
    # Probably change the approach to this
    law_words_list = generator(input_tokens)
    # Above is a 3d list.
    # Level 1: tokens
    # Level 2: synonym of token
    # Level 3: similar words
    query_string = ""
    token_first = True
    concatenater = ' OR ' if force_or == True else ' AND '
    for token_group in law_words_list:
        if not token_group: continue
        syns_string = "("
        syn_first = True
        for syn_group in token_group:
            if not syn_group: continue
            similars_string = "("
            similar_first = True
            for similar_word in syn_group:
                if not similar_word: continue
                if similar_first:
                    similars_string += similar_word
                    similar_first = False
                else:
                    similars_string += " OR " + similar_word
            similars_string += ")"
            if syn_first:
                syns_string += similars_string
                syn_first = False
            else:
                syns_string += ' OR ' + similars_string
        syns_string += ")"
        if token_first:
            query_string += syns_string
            token_first = False
        else:
            query_string += concatenater + syns_string

    return query_string


def query2lawsims(input_tokens, force_or=False):
    return _3dlist_to_query_lang(input_tokens, get_lawsimslist, force_or)


def query2lawsims_or(input_tokens, force_or=True):
    return _3dlist_to_query_lang(input_tokens, get_lawsimslist, force_or)


if __name__ == "__main__":
    import timeit

    # print(query2lawsims('murder a person'.split()))
    print(timeit.timeit(run, number=10))
    # print(run())
