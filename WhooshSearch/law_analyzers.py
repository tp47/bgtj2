from rake_nltk import Rake
from nltk.corpus import wordnet
import sqlite3
import re



def get_keywords(input_text):
    """
    Accept a string of text and return keyphrases.
    """
    r = Rake()
    r.extract_keywords_from_text(input_text)
    # To get keyword phrases ranked highest to lowest.
    return r.get_ranked_phrases()


def get_definitions(words):
    defs = {}
    for word in words:
        if re.match('[^a-zA-Z ]?', word):
            continue
        if len(word) < 5:
            continue
        syns = wordnet.synsets(word)
        if syns:
            defs[word] = syns[0].definition()
    return defs


def get_full_law_para(law_title, para_num, matched=None):
    DATABASE_URL = "../Databases/corpus_en.db"
    TABLE_NAME = "law_text"
    conn = sqlite3.connect(DATABASE_URL)
    if matched is None:
        matched = []
    c = conn.cursor()
    query = f"SELECT law_text from {TABLE_NAME} where law_name='{law_title}' and section='{para_num}'"
    all_text = ""
    res = c.execute(query)
    for row in res:
        text = row[0]
        if not (text in matched):
            all_text += "<br>" + text
        else:
            all_text += "<br> <mark>" + text + "</mark>"
    return all_text


def remove_tags(text):
    return re.sub('<[^>]*>', '', text)


def extract_punishments(law_text) -> list:
    """Extract punishments with a regex (very simple)"""
    matches = re.finditer(r'imprisonment(.*?)year((s)?)(( or a fine)?)', law_text)
    return [m.group() for m in matches]


def get_analysis(law_title, para_num):
    para_text = get_full_law_para(law_title, para_num)
    para_text_no_tags = remove_tags(para_text)
    keywords = get_keywords(para_text_no_tags)
    punishments = extract_punishments(para_text_no_tags)
    definitions = get_definitions(keywords)
    return {'keywords': keywords, 'punishments': punishments, 'definitions': definitions}


if __name__ == "__main__":
    print(get_analysis('stgb', '212'))
