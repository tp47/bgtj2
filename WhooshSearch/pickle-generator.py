from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
import pickle

stopWords = set(stopwords.words('english'))
def term_expander(_term, _pos=None, filter_syn_level = 1):
#     if term in stopWords:
#         return [term]
    all_terms = []
    word_syns = wn.synsets(_term, pos=_pos)
    for term in word_syns:
        term_word, pos, syn_level = term.name().split('.')
        if int(syn_level) > filter_syn_level:
            continue
        # all_terms.append(term_word)
        if term_word != _term:
            continue
        for hyper in term.hypernyms():
            hyper_word, pos, syn_level = hyper.name().split('.')
            if int(syn_level) > filter_syn_level:
                continue
            all_terms.append(hyper_word)
        # for lemma in a_syn.lemmas():
        #     all_terms.append(lemma.name())
    return list(set(all_terms))
# term_expander('murder')


def gen_rev_syns():
    dump_file = open('pickles/law_words.pickle','rb')
    law_words = pickle.load(dump_file)
    #generate all the synonyms
    law_syns = {}
    for a_word in law_words:
        syns = term_expander(a_word)
        law_syns[a_word] = syns
    law_syns_reverse = {}
    for law_word in law_syns.keys():
        for syn in law_syns[law_word]:
            if syn in law_syns_reverse.keys():
                law_syns_reverse[syn].append(law_word)
            else:
                law_syns_reverse[syn] = [law_word]
    return law_syns_reverse


lsr = gen_rev_syns()
from pprint import pprint
pprint(lsr.keys())
# print(term_expander('murder'))